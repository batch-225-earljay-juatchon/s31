let http = require("http");

const server = http.createServer((request, response) => {

	if (request.url == '/login') {

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Welcome to login page.");

	} else if (request.url == '/register') {

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("I'am sorry the page you are looking for cannot be found.");

	}

}).listen(3000);
console.log('Server running at localhost:3000')
